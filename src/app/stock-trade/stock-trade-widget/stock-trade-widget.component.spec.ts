import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StockMarketService } from '../../shared';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StockTradeWidgetComponent } from './stock-trade-widget.component';

describe('StockTradeWidgetComponent', () => {
  let component: StockTradeWidgetComponent;
  let fixture: ComponentFixture<StockTradeWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [StockMarketService],
      declarations: [ StockTradeWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTradeWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Company, GlobalQuote } from '../../shared/models';
import { Component, OnInit, Input } from '@angular/core';
import { StockMarketService } from '../../shared/stock-market.service';
import { TimeSeriesType } from '../../shared/models';

@Component({
  selector: 'app-stock-trade',
  templateUrl: './stock-trade-widget.component.html',
  styleUrls: ['./stock-trade-widget.component.scss']
})
export class StockTradeWidgetComponent implements OnInit {
  // TODO: Take selected company from
  @Input() company: Company;

  chartOptions = {
    responsive: true
  };
  chartData: any[];
  chartLabels: string[];
  globalQuote: GlobalQuote;

  constructor(private service: StockMarketService) { }

  ngOnInit(): void {

  }

  onSelected(timeSeries: TimeSeriesType) {

    this.service.getTimeSeriesBy(timeSeries)
      .subscribe(res => {
        this.chartData = res.dataSets;
        this.chartLabels = res.labels;
      });

  }

}

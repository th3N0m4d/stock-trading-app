import { Company } from './../../shared/models';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { StockMarketService } from '../../shared';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap'

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

  @Output() selected = new EventEmitter<Company>();
  results: Company[];
  searchTerm$ = new Subject<string>();

  constructor(private service: StockMarketService) { }

  ngOnInit() {
    this.service.searchCompanies(this.searchTerm$)
      .subscribe(results => this.results = results);
  }

  onSelected(result: any) {
    const selectedCompany = this.getSelected(result.target.value);
    this.selected.emit(selectedCompany);
  }

  private getSelected(company: string) {
    const symbol = company.match(/(\w+)/)[0];
    const selectedCompany = this.results.find(r => r.symbol === symbol);
    return selectedCompany;
  }
}

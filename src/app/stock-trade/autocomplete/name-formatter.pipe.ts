import { Company } from './../../shared/models';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameFormatter'
})
export class NameFormatterPipe implements PipeTransform {

  transform(company: Company): any {
    return `(${company.symbol}) ${company.name}`;
  }

}

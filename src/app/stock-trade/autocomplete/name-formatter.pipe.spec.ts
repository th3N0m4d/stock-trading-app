import { NameFormatterPipe } from './name-formatter.pipe';

describe('NameFormatterPipe', () => {
  it('create an instance', () => {
    const pipe = new NameFormatterPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return formatted companys title', ()=> {
    const pipe = new NameFormatterPipe();
    const company = {
      name: 'Heineken N.V.',
      symbol: 'HEINY',
      currency: 'USD'
    };

    expect(pipe.transform(company)).toBe('(HEINY) Heineken N.V.');
  });
});

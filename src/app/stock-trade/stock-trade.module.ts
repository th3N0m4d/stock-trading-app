import { ChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeriesOptionsWidgetComponent } from './series-options-widget/series-options-widget.component';
import { SharedModule } from '../shared/shared.module';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { NameFormatterPipe } from './autocomplete/name-formatter.pipe';
import { StockTradeWidgetComponent } from './stock-trade-widget/stock-trade-widget.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ChartsModule
  ],
  declarations: [
    StockTradeWidgetComponent,
    SeriesOptionsWidgetComponent,
    AutocompleteComponent,
    NameFormatterPipe,
  ],
  exports: [
    StockTradeWidgetComponent,
    SeriesOptionsWidgetComponent,
    AutocompleteComponent,
    SharedModule
  ],
})
export class StockTradeModule { }

import { TimeSeriesType, TimeSeries } from './../../shared/models';
import { Component, EventEmitter, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-series-options-widget',
  templateUrl: './series-options-widget.component.html'
})
export class SeriesOptionsWidgetComponent implements OnInit {

  @Output() selected = new EventEmitter<TimeSeriesType>();
  timeSeries: TimeSeries[] = [
    {
      type: TimeSeriesType.TIME_SERIES_DAILY,
      text: 'Daily',
      isSelected: true
    },
    {
      type: TimeSeriesType.TIME_SERIES_MONTHLY,
      text: 'Monthly',
      isSelected: false
    },
    {
      type: TimeSeriesType.TIME_SERIES_WEEKLY,
      text: 'Weekly',
      isSelected: false
    }
  ];

  onSelected(series: TimeSeries) {
    this.timeSeries.forEach(tm => tm.isSelected = false);

    series.isSelected = true;
    this.selected.emit(series.type);
  }

  ngOnInit(): void {
    this.onSelected(this.timeSeries[0]);
  }
}

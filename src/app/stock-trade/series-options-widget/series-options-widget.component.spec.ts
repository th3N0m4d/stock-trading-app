import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesOptionsWidgetComponent } from './series-options-widget.component';

describe('SeriesOptionsWidgetComponent', () => {
  let component: SeriesOptionsWidgetComponent;
  let fixture: ComponentFixture<SeriesOptionsWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule],
      declarations: [ SeriesOptionsWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesOptionsWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit Monthly series', ()=> {
    const spy = spyOn(component.selected, 'emit').and.callThrough();
    const selectedSeries = component.timeSeries[1];

    component.onSelected(selectedSeries);

    expect(spy).toHaveBeenCalledWith(selectedSeries.type);    
  });

  it('should select Monthly series only', ()=> {
    const selectedSeries = component.timeSeries[1];

    component.onSelected(selectedSeries);

    expect(component.timeSeries.find(ts=> ts.isSelected)).toEqual(selectedSeries);
  });
});

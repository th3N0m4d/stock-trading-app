import { Routes } from '@angular/router';
import { StockTradeWidgetComponent } from './stock-trade';

export const routes: Routes = [
    { path: '', component: StockTradeWidgetComponent },
    { path: '**', component: StockTradeWidgetComponent },
    { path: 'search/by/:symbol', component: StockTradeWidgetComponent },
    { path: 'search/by/:symbol/:time-series', component: StockTradeWidgetComponent },
];

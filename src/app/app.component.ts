import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Company, TimeSeriesType } from './shared';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styles: [
        `
        .navbar {
            margin-bottom: 20px;
        }
        `
    ]
})
export class AppComponent implements OnInit {

    selectedCompany: Company;
    selectedTimeSeries: TimeSeriesType;

    constructor(private router: Router) { }

    ngOnInit(): void {
        this.router.navigate(['search/by/', 'MSFT']);
    }

    onSelectedCompany(company: Company) {
        this.selectedCompany = company;
        this.router.navigate(['search/by/', company.symbol]);
    }

    onSelectedTimeSeries(timeSeries: TimeSeriesType) {
        this.selectedTimeSeries = timeSeries;
        this.router.navigate([
            'search/:symbol/:time-series',
            this.selectedCompany, this.selectedTimeSeries
        ]);
    }
}

import { tap } from 'rxjs/operators/tap';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { TimeSeriesType, Company, GlobalQuote } from './models';

@Injectable()
export class StockMarketService {
  private baseUrl = 'https://www.alphavantage.co/query?';
  private apiKey = 'B1G7H0I49MJPXSDC';

  constructor(private http: HttpClient) { }

  getTimeSeriesBy(type: TimeSeriesType, symbol: string = 'MSFT'): Observable<any> {
    return this.http
      .get(`${this.baseUrl}function=${TimeSeriesType[type]}&symbol=${symbol}&apikey=${this.apiKey}`)
      .pipe(
        map(res => {
          const series = Object.keys(res)[1];
          const labels = Object.keys(res[series]);
          const dataSets = [
            { data: labels.map(label => Number(res[series][label]['1. open'])), label: 'Open' },
            { data: labels.map(label => Number(res[series][label]['4. close'])), label: 'Close' },
          ];
          // TODO: Strongly type this object
          return {
            labels,
            dataSets
          };
        }),
        catchError(err => Observable.throw(err))
      );
  }

  searchCompanies(terms: Observable<string>) {
    return terms.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.getCompaniesBy(term));
  }

  getCompaniesBy(keyword: string): Observable<Company[]> {
    return this.http
      .get(`${this.baseUrl}function=SYMBOL_SEARCH&keywords=${keyword}&apikey=${this.apiKey}`)
      .pipe(
        map((res: any) => res.bestMatches.map(match=> { 
          return {
            name: match['2. name'],
            symbol: match['1. symbol'],
            currency: match['8. currency']
          }
        })),
        tap(res=> console.log(res)),
        catchError(err => Observable.throw(err))
      );
  }

  getGlobalQuote(symbol: string) : Observable<GlobalQuote> {
    return this.http
      .get(`${this.baseUrl}function=GLOBAL_QUOTE&symbol=${symbol}&apikey=${this.apiKey}`)
      .pipe(
        map(res=> {

          return {
            open: res['Global Quote']['02. open'],
            high: res['Global Quote']['03. high'],
            low: res['Global Quote']['04. low'],
            price: res['Global Quote']['05. price'],
            change: res['Global Quote']['09. change'],
            close: res['Global Quote']['08. previous close'],
            changePercent: res['Global Quote']['10. change percent']           
          }
        }), 
        tap(res=> console.log(res)),
        catchError(err => Observable.throw(err))
      );

  }


}

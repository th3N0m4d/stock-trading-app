import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StockMarketService } from './stock-market.service';

describe('StockMarketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [StockMarketService]
    });
  });

  it('should be created', inject([StockMarketService], (service: StockMarketService) => {
    expect(service).toBeTruthy();
  }));
});

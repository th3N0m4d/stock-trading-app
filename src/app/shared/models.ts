export enum TimeSeriesType {
    TIME_SERIES_WEEKLY,
    TIME_SERIES_MONTHLY,
    TIME_SERIES_DAILY
}

export interface TimeSeries {
    type: TimeSeriesType;
    text: string;
    isSelected: boolean;
  }

export interface Company {
    symbol: string;
    name: string;
    currency: string;
}

export interface GlobalQuote {
    open: number;
    high: number;
    low: number;
    close: number;
    price: number;
    change: number;
    changePercent: string;
}